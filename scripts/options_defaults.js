const DefaultOptions = {
  debug: false,
  autoDetach: false,
  autoDetachTaggedOnly: true,
  autoDetachTriggerTag: "ae_autoextract",
  defaultSavepath: ""
}
const OptionsList = Object.keys(DefaultOptions);

function defaultError(error) {
  console.error("AEC: Error:", error);
}