messenger.messages.listTags()
  .then(response => {
    consoleDebug("AEC: messenger.messages.listTags() response:", response);

    //check if "newtag" exists, if not add it to tag list

    function checkTagKey(tag) {
      return tag.key == this;
    }

    if (!response.find(checkTagKey, "ae_autoextract")) {
      var newMessageTag = {
          color: "black",
          key: "ae_autoextract", // Distinct tag identifier - use this string when referring to a tag
          tag: "AE AutoExtract", // Human-readable tag name
          ordinal: ""
      };

      // Add new tag to the tag list
      messenger.tagservice.AddTag(newMessageTag);

      consoleDebug("AEC: Add the trigger tag to the available tags");
    }

  })
