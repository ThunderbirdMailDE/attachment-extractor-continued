function saveOptions(e) {
  e.preventDefault();

  return messenger.storage.local.set({
    debug: document.getElementById("debug").checked,
    autoDetach: document.getElementById("autoDetach").checked,
    autoDetachTaggedOnly: document.getElementById("autoDetachTaggedOnly").checked,
    autoDetachTriggerTag: document.getElementById("autoDetachTriggerTag").value,
    defaultSavepath: document.getElementById("defaultSavepath").value
  });
}

function restoreOption(id) {
  return messenger.storage.local.get(id).then((res) => {
    consoleDebug("AEC: option.id: " + id);

  // radio handling
  /*
    if (id == "borderMode") {
      let elements = document.getElementsByName('borderMode');
      let num = res[id] || DefaultOptions[id];
      elements[num].checked = true;
    } else {
  */
      let element = document.getElementById(id);
      if (element.type && element.type == "checkbox") {
        consoleDebug( "AEC: restoreOption: " + element.id + " = " + res[id] );
        if (res[id] === undefined) {
          element.checked = DefaultOptions[id];
        } else {
          element.checked = res[id];
        }
      }
      else {
        consoleDebug( "AEC: restoreOption: " + element.id + " = " + res[id] );
        element.value = res[id] || DefaultOptions[id];
      }
   // }
  }, defaultError);
}

async function restoreAllOptions() {
  /*
    OptionsList.forEach((option) => {
      await restoreOption(option);
    });
  */

  await restoreOption("debug");
  await restoreOption("autoDetach");
  await restoreOption("autoDetachTaggedOnly");
  await restoreOption("autoDetachTriggerTag");
  await restoreOption("defaultSavepath");
  enableOrDisableOptionUiElements();
}

function resetAllOptions() {
  return messenger.storage.local.remove(OptionsList).then(() => {
    restoreAllOptions();
  });
}
