// From top of the HTML options page to bottom to be working correctly!
var parentAndChilds = [];
parentAndChilds = [
  ["autoDetach",["autoDetachTaggedOnly", "autoDetachTaggedOnlyLabelId", "autoDetachTriggerTag", "autoDetachTriggerTagLabelId"]],
  ["autoDetachTaggedOnly",["autoDetachTriggerTag", "autoDetachTriggerTagLabelId"]]
];

// Enable or disable options UI child elements
function enableOrDisableOptionUiElements() {
  parentAndChilds.forEach((parentAndChild) => {
    consoleDebug("AEC: parentElement: " + parentAndChild[0]);
    if ((!document.getElementById(parentAndChild[0]).checked) || (document.getElementById(parentAndChild[0]).getAttribute("disabled"))) {
      parentAndChild[1].forEach((childElement) => {
        consoleDebug("AEC: disable child: " + childElement);
        if ((document.getElementById(childElement).nodeName == "INPUT") || (document.getElementById(childElement).nodeName == "SELECT")) {
          document.getElementById(childElement).setAttribute("disabled", "true");
        }
        else {
          consoleDebug("AEC: disable child: per classList.add: " + childElement);
          document.getElementById(childElement).classList.add("disabled");
        }
      });
    } else {
      parentAndChild[1].forEach((childElement) => {
        consoleDebug("AEC: enable child: " + childElement);
        if ((document.getElementById(childElement).nodeName == "INPUT") || (document.getElementById(childElement).nodeName == "SELECT")) {
          document.getElementById(childElement).removeAttribute("disabled");
        }
        else {
          consoleDebug("AEC: enable child: per classList.remove: " + childElement);
          document.getElementById(childElement).classList.remove("disabled");
        }
      });
    }
  });
}

// Fill the autoDetachTagsList
async function fillAutoDetachTagSelectOptions() {
  let tagList = await browser.messages.listTags();
  let select = document.getElementById("autoDetachTriggerTag");

  for (let i = 0; i < tagList.length; i++){
    var opt = document.createElement("option");
    opt.value = tagList[i].key;
    opt.innerHTML = tagList[i].tag;
    select.appendChild(opt);
  }
}

// Browse for default save path and store it
async function browseDefaultSavepath() {
  let requestedWindowID = await messenger.windows.getCurrent();
  consoleDebug("AEC: window: " + requestedWindowID.id);

  let folderPath = await messenger.storage.local.get("defaultSavepath").then((res) => {
    return res["defaultSavepath"] || DefaultOptions["defaultSavepath"];
  });
  consoleDebug("AEC: folderPath: " + folderPath);

  let selectedFolderPath = await messenger.filePicker.browseForFolder(folderPath, "Ordner auswählen", requestedWindowID.id, options.debug);
  consoleDebug("AEC: selectedFolderPath: " + selectedFolderPath);

  if (selectedFolderPath) {
    await messenger.storage.local.set({
      defaultSavepath: selectedFolderPath,
    });
    await restoreOption("defaultSavepath");
  }
}
