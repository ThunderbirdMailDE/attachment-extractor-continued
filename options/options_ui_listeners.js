// onLoad listener to build the UI and Preview
document.addEventListener('DOMContentLoaded', () => {
  consoleDebug("AEC: DOMContentLoaded, restoreAllOptions()");
  fillAutoDetachTagSelectOptions()
  .then(() => { restoreAllOptions(); })
//  .then(() => { enableOrDisableOptionUiElements(); })
  ;
});

// onChange listener for all options to save the changed options
OptionsList.forEach((option) => {
  consoleDebug("AEC: option: " + option);
  /*
  if (option == "borderMode") {
    document.getElementById("bordermode0").addEventListener("change", (e) => {
      consoleDebug("AEC: Option borderMode-0 changed, saveOptions()");
      saveOptions(e)
      .then(() => { updatePreview(); });
    });
    document.getElementById("bordermode1").addEventListener("change", (e) => {
      consoleDebug("AEC: Option borderMode-1 changed, saveOptions()");
      saveOptions(e)
      .then(() => { updatePreview(); });
    });
  } else {
  */
    document.getElementById(option).addEventListener("change", (e) => {
      consoleDebug("AEC: Other Option changed, saveOptions()");
      saveOptions(e);
      enableOrDisableOptionUiElements();
    });
  //  }
});

// browse_defaultSavepath click listener
document.getElementById("browse_defaultSavepath").addEventListener("click", () => {
  consoleDebug("AEC: browse_defaultSavepath clicked");
  browseDefaultSavepath();
});

// reset click listener
document.getElementById("reset").addEventListener("click", () => {
  consoleDebug("AEC: Reset clicked, resetAllOptions()");
  resetAllOptions();
});

// onChange listeners to invoke setAttributeDisabled for nested options
/* Disabled, due to the fact, that listeners just exist from the options itself 
function registerOptionUiListeners() {
  parentAndChilds.forEach((parentAndChild) => {
    consoleDebug("AEC: Add Listeners: parentElement: " + parentAndChild[0]);
    document.getElementById(parentAndChild[0]).addEventListener("change", (e) => {
      enableOrDisableOptionUiElements();
    });
  });
}
*/
