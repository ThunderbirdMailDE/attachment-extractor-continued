***AttachmentExtractor Continued***

--------

### Features

* This Thunderbird Add-on is a (work in progress) fixed version of the original Add-on "AttachmentExtractor" which is no longer supported and fixed by the original author. It extracts all attachments from selected messages and then can delete, detach and more.
* Some features have been removed out of this addons options dialog from up version 3 (Thunderbird 78) because of not being functional. However, the affected features did not work in the past version series (1.5.* and 2.*). So they do not represent a new loss.
* Unfortunately, this revised version of the original Add-on will no longer be fully functional with future Thunderbird releases and will no longer be possible with all its features as a Web-/MailExtension in the future. Perhaps there will at least be a limited version for future Thunderbird releases.

### Version series

* Version 4.*   - Thunderbird 91.* - given up until some core changes in Thunderbird have been implemented
* Version 3.*   - Thunderbird 78.*
* Version 2.*   - Thunderbird 68.*
* Version 1.5.* - Thunderbird 60.*

### Known issues

* When detaching/deleting attachments from multiple messages, there will be no links from the stripped messages to the saved attachments. To get this working we would need a small patch in Thunderbirds core, which was/is ignored by the core developers: RFE [Bug 1578801](https://bugzilla.mozilla.org/show_bug.cgi?id=1578801).

### Installation

1. [Download AttachmentExtractor Continued from the official Thunderbird add-on page](https://addons.thunderbird.net/addon/attachmentextractor-continued/)
2. [Installing an Add-on in Thunderbird](https://support.mozilla.org/kb/installing-addon-thunderbird)


### Contribution

You are welcomed to contribute to this project by:
* adding or improving the localizations of AttachmentExtractor Continued via email to me or a post in german Thunderbird forums [Thunderbird Mail DE](https://www.thunderbird-mail.de/forum/board/81-hilfe-und-fehlermeldungen-zu-thunders-add-ons/) or just create an [issue](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/) here on GitLab
* creating [issues](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/) about problems
* creating [issues](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/issues/) about possible improvements


### Coders

* Alexander Ihrig (Maintainer)
* Andrew Williamson (Original Author)
* thestonehead (WebExtension Code)
* Sesu8642 (Code improvements for dealing with large amounts of messages)

### Translators

* 


### License

[Mozilla Public License version 2.0](https://gitlab.com/ThunderbirdMailDE/attachment-extractor-continued/LICENSE)